package com.saqlain.springbootmongodb.controller;

import com.saqlain.springbootmongodb.entity.Student;
import com.saqlain.springbootmongodb.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @PostMapping("addStudent")
    public Student addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    @GetMapping("getStudentByUserName")
    public Student getStudentByUserName(@RequestParam String userName) {
        return studentService.getStudentByUserName(userName);
    }

    @GetMapping("getAllStudents")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @PutMapping("updateStudent")
    public Student updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    @DeleteMapping("removeStudent")
    public String removeStudent(@RequestParam String userName) {
        return studentService.removeStudent(userName);
    }

}
