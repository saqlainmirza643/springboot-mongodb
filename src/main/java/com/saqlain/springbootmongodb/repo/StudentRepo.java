package com.saqlain.springbootmongodb.repo;

import com.saqlain.springbootmongodb.entity.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepo extends MongoRepository<Student, Integer> {
    Student findByUserName(String userName);
}
