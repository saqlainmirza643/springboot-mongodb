package com.saqlain.springbootmongodb.service;

import com.saqlain.springbootmongodb.entity.Student;

import java.util.List;

public interface StudentService {
    Student addStudent(Student student);

    Student getStudentByUserName(String userName);

    List<Student> getAllStudents();

    Student updateStudent(Student student);

    String removeStudent(String userName);
}
