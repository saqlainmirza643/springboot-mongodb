package com.saqlain.springbootmongodb.service.impl;

import com.saqlain.springbootmongodb.entity.Student;
import com.saqlain.springbootmongodb.repo.StudentRepo;
import com.saqlain.springbootmongodb.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepo studentRepo;

    @Override
    public Student addStudent(Student student) {
        return studentRepo.save(student);
    }

    @Override
    public Student getStudentByUserName(String userName) {
        return studentRepo.findByUserName(userName);
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepo.findAll();
    }

    @Override
    public Student updateStudent(Student student) {
        return studentRepo.save(student);
    }

    @Override
    public String removeStudent(String userName) {
        Student student = studentRepo.findByUserName(userName);
        studentRepo.delete(student);
        return "deleted";
    }
}
